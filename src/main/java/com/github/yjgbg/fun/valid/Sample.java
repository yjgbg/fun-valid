package com.github.yjgbg.fun.valid;

import com.github.yjgbg.fun.valid.core.Validator;
import com.github.yjgbg.fun.valid.ext.LbkExtValidatorsCore;
import com.github.yjgbg.fun.valid.ext.LbkExtValidatorsStd;
import io.vavr.collection.Stream;
import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.ExtensionMethod;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.concurrent.Executors;

// 使用ExtensionMethod注解引入这两个扩展类
@ExtensionMethod({LbkExtValidatorsCore.class, LbkExtValidatorsStd.class})
public class Sample {
	public static void main(String[] args) {
		final var entity1 = new Person("null", 0L, Gender.FEMALE, "1234567891011", "asdqq.com", null);
		final var validator = Validator.<Person>none()
				.nonNull("person不能为空")
				.gt(Person::getId, "id应该大于0", false, 0L)
				.length(Person::getName, "name应该是2-5个字", false, 2, 5)
				.and(Person::getName, "block", name -> {
					try {
						Thread.sleep(20);
					} catch (Exception ignored) {
					}
					return true;
				})
				.equal(Person::getGender, "应该是男性", Gender.MALE)
				.regexp(Person::getPhone, "电话号码不合法", true, "^\\d{11}$")
				.regexp(Person::getEmail, "email不合法", false,
						"^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$")
				.iter();
		final var list = Stream.fill(200,entity1).toJavaList();
		// 实测结果基本维持了随线程数线性增长的性能表现
		final var cached = Executors.newCachedThreadPool();
		final var exe32 = Executors.newFixedThreadPool(32);
		final var exe16 = Executors.newFixedThreadPool(16);
		final var exe8 = Executors.newFixedThreadPool(8);
		final var exe4 = Executors.newFixedThreadPool(4);
		benchmark(() -> validator.apply(cached, false, list).get(), "cached"); // cached:492
		benchmark(() -> validator.apply(exe32, false, list).get(), "32thread"); // 32thread:2221
		benchmark(() -> validator.apply(exe16, false, list).get(), "16thread"); // 16thread:4113
		benchmark(() -> validator.apply(cached, false, list).get(), "cached"); // cached:318
		benchmark(() -> validator.apply(exe32, false, list).get(), "32thread"); // 32thread:2205
		benchmark(() -> validator.apply(exe16, false, list).get(), "16thread"); // 16thread:4102
		benchmark(() -> validator.apply(cached, false, list).get(), "cached"); // cached:337
		benchmark(() -> validator.apply(exe8, false, list).get(), "8thread"); // 8thread:7892
		benchmark(() -> validator.apply(exe4, false, list).get(), "4thread"); // 4thread:15788
		benchmark(() -> validator.apply(false, list), "normal"); // normal:63169
	}
	public static void benchmark(Runnable r, String name) {
		final var start = System.currentTimeMillis();
		for (int i = 0; i < 10; i++) {
			r.run();
		}
		System.out.println(name + ":" + (System.currentTimeMillis() - start));
	}
}

@Data
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class Person {
	String name;
	Long id;
	Gender gender;
	String phone;
	String email;
	List<Person> children;
}

enum Gender {
	MALE, FEMALE
}
