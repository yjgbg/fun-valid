package com.github.yjgbg.fun.valid.core;

import io.vavr.Function3;
import io.vavr.concurrent.Future;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.Executor;
import java.util.function.Function;

import static io.vavr.API.Future;

/**
 * Validator接口,以及Validator之间的运算
 *
 * @param <A> 校验的目标元素类型
 */
@FunctionalInterface
public interface Validator<A> extends Function3<@NotNull Executor,@NotNull Boolean, @Nullable A, Future<Errors>> {

	default Errors apply(@NotNull Boolean failFast, @Nullable A obj) {
		return apply(Runnable::run, failFast, obj).getOrElse(Errors.none());
	}
	@Override
	Future<Errors> apply(@NotNull Executor exe, @NotNull Boolean failFast, @Nullable A obj);

	/**
	 * 空校验器
	 * 构造一个校验结果恒为空的校验器
	 *
	 * @param <A> 校验的目标元素类型
	 * @return 校验结果恒为空的校验器
	 */
	static <A> Validator<A> none() {
		return (exe, failFast, obj) -> Future(exe, Errors.none());
	}

	/**
	 * 简单校验器
	 * 根据constraint描述的规则，和message描述的错误信息构造一个简单校验器
	 *
	 * @param <A>        校验的目标元素类型
	 * @param message    错误信息
	 * @param constraint 约束条件表达式
	 * @return 简单校验器
	 */
	static <A> Validator<A> simple(Function<@Nullable A, @NotNull Boolean> constraint, Function<A, String> message) {
		return (exe, failFast, obj) -> Future(exe, () -> constraint.apply(obj) ? Errors.none() : Errors.simple(obj, message.apply(obj)));
	}

	/**
	 * A类复杂校验器
	 * 根据两个目标元素的校验器，构造一个目标元素的校验器
	 *
	 * @param <A>        校验的目标元素类型
	 * @param validator0 一个校验器
	 * @param validator1 另一个校验器
	 * @return 由两个校验器组合而成的校验器
	 */
	static <A> Validator<A> plus(Validator<? super A> validator0, Validator<? super A> validator1) {
		return (exe, failFast, obj) -> {
			final var res = validator0.apply(exe, failFast, obj);
			if (failFast) return res.filter(Errors::hasError).orElse(validator1.apply(exe, true, obj));
			else return res.zipWith(validator1.apply(exe, false, obj), Errors::plus);
		};
	}

	/**
	 * 将目标元素校验器构造器映射为校验器
	 *
	 * @param validatorFunction 校验器构造器
	 * @param <A>               校验的目标元素类型
	 * @return 校验器
	 */
	static <A> Validator<A> from(Function<A, Validator<? super A>> validatorFunction) {
		return (exe, failFast, obj) -> validatorFunction.apply(obj).apply(exe, failFast, obj);
	}

	/**
	 * B类复杂校验器
	 * 根据field类型的校验器和prop，构造一个目标类型校验器,规则为校验目标元素经过getter运算之后的结果符合validator校验
	 *
	 * @param <A>       类型A
	 * @param <B>       类型B
	 * @param validator 一个B类型的转换器
	 * @param prop      一个A 到 B的转换器
	 * @return 目标类型校验器
	 */
	static <A, B> Validator<A> transform(Getter<A, @Nullable B> prop, Validator<? super B> validator) {
		return (exe, failFast, obj) -> validator.apply(exe, failFast, prop.apply(obj))
				.map(err -> Errors.transform(prop.propertyName(), err));
	}
}
