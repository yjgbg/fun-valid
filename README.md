### 前置知识
[Lombok-ExtensionMethod](https://projectlombok.org/features/experimental/ExtensionMethod)

[plugin](https://github.com/mplushnikov/lombok-intellij-plugin/files/5505383/lombok-plugin-0.34-EAP.zip)
### 
```xml
<dependency>
    <groupId>com.github.yjgbg</groupId>
    <artifactId>fun-valid</artifactId>
    <version>ROLLING-SNAPSHOT</version>
</dependency>
```

[javadoc](https://com-github-yjgbg.github.io/fun-valid)
